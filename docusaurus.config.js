// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Phinansal Özgürlük',
  tagline: '',
  url: 'https://phinansalozgurluk.com',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'phinansalozgurluk', // Usually your GitHub org/user name.
  projectName: 'phinansalozgurluk', // Usually your repo name.

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl: 'https://bitbucket.org/phinansalozgurluk/websitesi/src/master/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://bitbucket.org/phinansalozgurluk/websitesi/src/master/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      navbar: {
        title: 'Phinansal Özgürlük',
        logo: {
          alt: 'Phinansal Özgürlük Logo',
          src: 'img/logo.svg',
        },
        items: [
          {
            type: 'doc',
            docId: 'intro',
            position: 'left',
            label: 'Bilgi Merkezi',
          },
          {to: '/blog', label: 'Blog', position: 'left'},
          {
            href: 'https://www.youtube.com/channel/UC9StWWclCdToX9uSLhc4kxQ',
            label: 'Youtube',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Docs',
            items: [
              {
                label: 'Bilgi Merkezi',
                to: '/docs/intro',
              },
            ],
          },
          {
            title: 'Community',
            items: [
              // {
              //   label: 'Stack Overflow',
              //   href: 'https://stackoverflow.com/questions/tagged/phinansalozgurluk',
              // },
              // {
              //   label: 'Discord',
              //   href: 'https://discordapp.com/invite/phinansalozgurluk',
              // },
              {
                label: 'Twitter',
                href: 'https://twitter.com/phiozgurluk',
              },
            ],
          },
          {
            title: 'More',
            items: [
              {
                label: 'Blog',
                to: '/blog',
              },
              {
                label: 'Bitbucket',
                href: 'https://bitbucket.org/phinansalozgurluk/',
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} Phinansalozgurluk, Inc. Built with Docusaurus.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
